var gulp = require('gulp');
var config = require('../config');

gulp.task('data', () => {
  return gulp.src('./src/data/*.json')
    .pipe(gulp.dest(config.buildDir + '/data'));
});