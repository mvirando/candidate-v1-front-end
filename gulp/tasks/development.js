'use strict';

var gulp = require('gulp');
var runSequence = require('run-sequence');

gulp.task('dev', ['jscs', 'jscpd', 'lint', 'clean'], cb => {

  global.isProd = false;
  global.isServerDeploy = false;

  runSequence(['data', 'templates', 'styles', 'fonts', 'images', 'browserify', 'svg'], 'watch', cb);

});
