'use strict';

var config = require('../config');
var browserSync = require ('browser-sync');
var request = require('request');
var gulp = require ('gulp');
var url = require('url');
var proxy = require('proxy-middleware');
var proxyOptions = url.parse('http://dev.mip.coop.clientsofradical.com/api');
    proxyOptions.route = '/api';
    proxyOptions.via = true;

gulp.task('browserSync', () => {
  browserSync({
    port: config.browserPort,
    ui: {
      port: config.UIPort
    },
    open: false,
    server: {
      baseDir: './pub',
      middleware: [proxy(proxyOptions)]
    }
  });
});