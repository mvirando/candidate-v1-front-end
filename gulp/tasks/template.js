'use strict';

var config = require('../config');
var gulp = require('gulp');
var browserSync = require('browser-sync');
var merge = require('merge-stream');
var templateCache = require('gulp-angular-templatecache');

gulp.task('templates', function() {

  const indexFile = gulp.src(config.templates.index)
    .pipe(gulp.dest(config.buildDir));

  const templates = gulp.src(config.templates.src)
    .pipe(templateCache({
      standalone: true
    }))
    .pipe(gulp.dest(config.templates.dest))
    .pipe(browserSync.stream());

  return merge(indexFile, templates);

});
