'use strict';

var config = require('../config');
var changed = require('gulp-changed');
var gulpif = require('gulp-if');
var gulp = require('gulp');
var browserSync = require('browser-sync');

gulp.task('svg', function() {

  return gulp.src(config.svg.src)

    .pipe(changed(config.svg.dest)) // Ignore unchanged files
    .pipe(gulp.dest(config.svg.dest))
    .pipe(gulpif(!global.isServerDeploy, browserSync.stream()));

});
