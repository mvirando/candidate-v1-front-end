import gulp from 'gulp';
import eslint from 'gulp-eslint';
import config from '../config';

gulp.task('lint', () => {
 return gulp.src(config.js.src)
  .pipe(eslint())
  .pipe(eslint.format())
});