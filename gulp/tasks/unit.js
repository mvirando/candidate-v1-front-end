'use strict';

var config = require('../config');
var path = require('path');
var gulp = require('gulp');
var karma = require('karma');

gulp.task('unit', ['templates'], function(cb) {
  new karma.Server({
    configFile: path.resolve(__dirname, '../..', config.test.karma),
    singleRun: true
  }, cb).start();
});