'use strict';

var config = require('../config');
var gulp = require('gulp');

gulp.task('watch', ['browserSync'], () => {

  global.isWatching = true;

  gulp.watch(config.scripts.src, ['lint']);
  gulp.watch(config.styles.src,  ['styles']);
  gulp.watch(config.templates.watch, ['templates']);
  gulp.watch(config.js.src, ['jscpd', 'jscs', 'lint']);

});