var gulp = require('gulp');
var config = require('../config');
var jscpd = require('gulp-jscpd');

gulp.task('jscpd', () => {
  return gulp.src(config.js.src)
    .pipe(jscpd({
      verbose: true,
      'min-lines': 15
    }));
});