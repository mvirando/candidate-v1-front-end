import gulp from 'gulp';
import jscs from 'gulp-jscs';
import config from '../config';
import stylish from 'gulp-jscs-stylish';
import notify from 'gulp-notify';
import plumber from 'gulp-plumber';

gulp.task('jscs', () => {
  return gulp.src(config.js.src)
    .pipe(plumber({
        errorHandler: notify.onError({
          title: "JSCS error!",
        })
      })
    )
    .pipe(jscs())
    .pipe(jscs.reporter('failImmediately'))
    .pipe(stylish());
});
