'use strict';

var gulp = require('gulp');
var runSequence = require('run-sequence');

gulp.task('deploy-local', ['jscs', 'jscpd', 'lint', 'clean'], function (cb) {

  global.isProd = true;
  global.isServerDeploy = true;

  runSequence(['data', 'templates', 'styles', 'fonts', 'images', 'browserify'], process.exit);

});