'use strict';

var config = require('../config');
var testServer = require('../util/testServer');
var gulp = require('gulp');
var angularProctractor = require('gulp-angular-protractor');

gulp.task('protractor', function(cb) {

  var testFiles = gulp.src('test/e2e/**/*.e2e.js');

  testServer(config.browserPort, config.buildDir).then(function(server) {
    testFiles.pipe(angularProctractor({
      configFile: config.test.protractor,
      autoStartStopServer: true,
      debug: true
    })).on('error', function(err) {
      throw err;
    }).on('end', function() {
      server.close(cb)
      process.exit();
    });
  });

});