'use strict';

module.exports = {
  'browserPort': 3000,
  'UIPort': 3001,
  'serverPort': 3002,
  'buildDir': './pub',
  'sourceDir': './src/',
  'browserify': {
    'entries'   : ['./src/index.js'],
    'bundleName': 'bundle.js',
    'sourcemap' : true
  },
  'scripts': {
    'dest': './pub/js'
  },
  'styles': {
    'src': ['./src/scss/**/*.scss','./src/views/**/*.scss','./src/components/**/*.scss' ],
    'dest': './pub/css'
  },
  'dist': {
    'root'  : './pub'
  },
  images: {
    src: ['./src/assets/images/**/*','./src/assets/images/*'],
    dest: './pub/images'
  },

  fonts: {
    src: './src/assets/fonts/**/**/*',
    dest: './pub/fonts'
  },

  'svg': {
    src: './src/assets/svg/*',
    dest: './pub/svg'
  },

  'templates': {
    'index': './src/index.html',
    'src': ['./src/**/**/*.html'],
    'dest': './src/'
  },

  js: {
    'src': [
      './src/**/**/*.js',
      '!./src/assets',
      '!./src/scss',
      '!./src/templates.js'
    ]
  },

  test: {
    karma: 'test/karma.conf.js',
    protractor: 'test/protractor.conf.js'
  },

  init: function() {
    this.templates.watch = [
      this.templates.index,
      this.templates.src
    ];
    return this;
  }
}.init();
