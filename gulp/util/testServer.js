
'use strict';

var express = require('express');

module.exports = function testServer(port, dir) {

  var app = express();

  app.use(express.static(dir));

  return new Promise(function(res, rej) {
    var server = app.listen(port, function() {
      return res(server);
    });
  });

}