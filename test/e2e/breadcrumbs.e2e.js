/*global browser */

'use strict';

describe('E2E: Breadcrumbs', function() {

  it('should show "Step 1 of 5" on the "Introduction Page".', function() {
    browser.get('#/');
    expect(element(by.css('.c-Breadcrumbs__step')).getText()).toMatch(/Step 1 of 5/);
  });

  it('should show "Step 2 of 5" on the "Your mortgage needs".', function() {
    browser.get('#/2/your-mortgage-needs');
    expect(element(by.css('.c-Breadcrumbs__step')).getText()).toMatch(/Step 2 of 5/)
    expect(element(by.css('.c-Breadcrumbs__link')).isDisplayed()).toBe(true);
  });

  it('should show "Step 3 of 5" on the "About You" page.', function() {
    browser.get('#/3/about-you');
    expect(element(by.css('.c-Breadcrumbs__step')).getText()).toMatch(/Step 3 of 5/)
    expect(element(by.css('.c-Breadcrumbs__link')).isDisplayed()).toBe(true);
  });

  it('should show "Step 4 of 5" on the "Your income" page.', function() {
    browser.get('#/4/your-income');
    expect(element(by.css('.c-Breadcrumbs__step')).getText()).toMatch(/Step 4 of 5/)
    expect(element(by.css('.c-Breadcrumbs__link')).isDisplayed()).toBe(true);
  });

});