/*global browser */

'use strict';

describe('E2E: About You E2E', function() {

  beforeAll(function() {
    browser.get('#/3/about-you');
  });

  describe('should change all the fields and submit...', function() {

    /*-- Title --*/
    it('select the Mr "Title".', function() {
      var customerType = element(by.model('vm.data.primary.title'));
      selectDropdownByNumber(customerType, 1);
      expect(customerType.$('option:checked').getText()).toEqual('Mr');
    });

    /*-- Name --*/
    it('add first name', function() {
      var $input = element(by.model('vm.data.primary.firstName'));
      $input.sendKeys('John');
      expect($input.getAttribute('value')).toEqual('John');
    });

    it('add surname', function() {
      var $input = element(by.model('vm.data.primary.surname'));
      $input.sendKeys('Smith');
      expect($input.getAttribute('value')).toEqual('Smith');
    });

    /*-- Date of birth --*/
    it('add date of birth day.', function() {
      var $input = element(by.model('vm.data.primary.dateOfBirth.day'));
      $input.sendKeys('17');
      expect($input.getAttribute('value')).toEqual('17');
    });

    it('add date of birth month.', function() {
      var $input = element(by.model('vm.data.primary.dateOfBirth.month'));
      $input.sendKeys('09');
      expect($input.getAttribute('value')).toEqual('09');
    });

    it('add date of birth year.', function() {
      var $input = element(by.model('vm.data.primary.dateOfBirth.year'));
      $input.sendKeys('1987');
      expect($input.getAttribute('value')).toEqual('1987');
    });

    /*-- Email --*/
    it('add email', function() {
      var $input = element(by.model('vm.data.primary.emailAddress'));
      $input.sendKeys('johnsmith@test.com');
      expect($input.getAttribute('value')).toEqual('johnsmith@test.com');
    });

    /*-- Post Code --*/
    // it('find the address via the postcode input', function() {
    //   var $input = element(by.model('postCode.data.postCodeServiceStore.enteredPostcode'));
    //   $input.sendKeys('LS1 2AA');
    //   element($(''))
    //   expect(element($('label[for="postcode"]').isDisplayed)).toBeTruthy();
    //   // expect($input.getAttribute('value')).toEqual('johnsmith@test.com');
    // });

    const selectDropdownByNumber = function(element, i) {
      if (i) {
        var options = element.getWebElement().findElements(by.tagName('option'))
          .then(function (options) {
            options[i].click();
          });
      }
    }

  });

});