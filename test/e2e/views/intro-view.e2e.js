/*global browser */

'use strict';

describe('E2E: IntroView E2E', function() {

  beforeAll(function() {
    browser.get('#/');
  });

  describe('should change get all the relevant fields and submit...', function() {

    it('should have the "customer type" select.', function() {
      var requestType = element.all(by.model('vm.data.requestType')).get(0);
      requestType.click();
      expect(requestType.isSelected()).toBe(true);
    });

    it('should select the "First Time Buyer" in the customer type.', function() {
      var customerType = element(by.model('vm.data.prospectType'));
      selectDropdownByNumber(customerType, 1);
      expect(customerType.$('option:checked').getText()).toEqual('First Time Buyer');
    });

    it('should sumbit to the next page.', function() {
      element(by.css('.c-btn--primary')).click().then(function() {
        expect(browser.getCurrentUrl()).toEqual('http://localhost:8000/#/2/your-mortgage-needs');
      });
    });

    const selectDropdownByNumber = function(element, i) {
      if (i) {
        var options = element.getWebElement().findElements(by.tagName('option'))
          .then(function (options) {
            options[i].click();
          });
      }
    }

  });

});