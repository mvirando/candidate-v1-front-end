/*global browser */

'use strict';

describe('E2E: Your Income E2E', function() {

  beforeAll(function() {
    browser.get('#/4/your-income');
  });

  describe('should change get all the relevant fields and submit...', function() {

    it('select the employment status".', function() {
      var employmentStatus = element(by.model('vm.data.income.employmentStatus'));
      selectDropdownByNumber(employmentStatus, 1);
      expect(employmentStatus.$('option:checked').getAttribute('value')).toEqual('EMPLOYED');
    });

    const selectDropdownByNumber = function(element, i) {
      if (i) {
        var options = element.getWebElement().findElements(by.tagName('option'))
          .then(function (options) {
            options[i].click();
          });
      }
    }

  });

});