/*global browser */

'use strict';

describe('E2E: Your Mortgage Needs E2E', function() {

  beforeAll(function() {
    browser.get('#/2/your-mortgage-needs');
  });

  describe('set the mortgage value', function() {

    it('and format the numbers', function() {
      var $input = element(by.model('vm.data.propertyValue'));
      $input.sendKeys('150000');
      element(by.css('body')).click().then(function() {
        expect($input.getAttribute('value')).toEqual('£150,000');
      });
    });

    it('and submit move to the next page', function() {
      element(by.css('.c-btn--primary')).click().then(function() {
        expect(browser.getCurrentUrl()).toEqual('http://localhost:8000/#/3/about-you');
      });
    });

  });
});