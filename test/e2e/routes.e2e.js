/*global browser */

'use strict';

describe('E2E: Routes', function() {

  /* 1 */
  it('should have a working home route', function() {
    browser.get('#/');
    expect(browser.getLocationAbsUrl()).toMatch('/');
  });

  /* 2 */
  it('should navigate to the "your mortgage needs" page.', function() {
    browser.get('#/2/your-mortgage-needs');
    expect(browser.getLocationAbsUrl()).toMatch('/2/your-mortgage-needs');
  });

  /* 3 */
  it('should navigate to the "about you" page.', function() {
    browser.get('#/3/about-you');
    expect(browser.getLocationAbsUrl()).toMatch('/3/about-you');
  });

  /* 4 */
  it('should navigate to the "your income" page.', function() {
    browser.get('#/4/your-income');
    expect(browser.getLocationAbsUrl()).toMatch('/4/your-income');
  });

});