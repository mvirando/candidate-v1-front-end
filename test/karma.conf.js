'use strict';

var istanbul = require('browserify-istanbul'),
    isparta  = require('isparta'),

karmaBaseConfig = {
  basePath: '../',
  colors: true,
  autoWatch: true,
  singleRun: true,
  frameworks: ['jasmine', 'browserify'],
  preprocessors: {
    './src/**/**/*.js': ['browserify', 'coverage']
  },
  babelPreprocessor: {
    options: {
      presets: ['es2015'],
      sourceMap: 'inline'
    },
    filename: function (file) {
      return file.originalPath.replace(/\.js$/, '.es5.js');
    },
    sourceFileName: function (file) {
      return file.originalPath;
    }
  },
  browsers: ['PhantomJS'],
  reporters: ['progress', 'coverage'],
  browserify: {
    debug: true,
    extensions: ['.js'],
    transform: [
      'babelify',
      'browserify-ngannotate',
      'bulkify',
      istanbul({
        instrumenter: isparta,
        ignore: ['**/node_modules/**', '**/test/**']
      })
    ]
  },
  port: 8081,
  files: [
    './src/index.js',
    'node_modules/angular-mocks/angular-mocks.js',
    'node_modules/moment/moment.js',
    'test/unit/**/**/*.js'
  ],
  coverageReporter: {
    type: 'html',
    dir : './test/coverage'
  }
};

module.exports = function(config) {
  config.set(karmaBaseConfig);
};