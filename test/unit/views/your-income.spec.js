/* global angular */

'use strict';

describe('UNIT: Your Income Spec', function() {

  var controller, form, state, status = 'EMPLOYED';

  beforeEach(function() {

    angular.mock.module('app');
    angular.mock.inject(function($rootScope, $controller, $templateCache, $compile, appState) {
      controller = $controller('yourIncomeController');
      controller.data.employmentStatuses = status;
      state = appState;
    });

  });

  it('should exist', function() {
    expect(controller).toBeDefined();
  });

  it('vm.submit should post the data', function() {
    /*--- Setup ---*/
    var mockEvent = new Event('click');
    var isValid = true;
    spyOn(mockEvent, 'preventDefault');
    controller.onSubmit(mockEvent, isValid);
    /*--- Test ---*/
    expect(mockEvent.preventDefault).toHaveBeenCalled();
    expect(state.state.primary.employmentStatuses).toEqual(status);
  });

});