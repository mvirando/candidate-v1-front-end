/* global angular */

'use strict';

describe('UNIT: Intro Spec', function() {

  var controller,
    form,
    state,
    location,
    requestTypeValue = 'SINGLE',
    prospectTypeValue = 'REMORTGAGER',
    killerRequestType = 'MORE',
    killerprospectType = 'EXISTING';

  beforeEach(function() {

    angular.mock.module('app');
    angular.mock.inject(function($controller, appState, $location) {
      controller = $controller('introController');
      location = $location;
      state = appState;
      spyOn(location, 'path').and.callThrough();
    });

  });

  it('controller should exist', function() {
    expect(controller).toBeDefined();
  });

  // it('vm.submit should save the data locally and proceed', function() {
  //   /*--- Setup ---*/
  //   controller.data.requestType = requestTypeValue;
  //   controller.data.prospectType = prospectTypeValue;
  //   var valid = true;
  //   controller.onSubmit(valid);
  //   /*--- Test ---*/
  //   expect(state.state.requestType).toEqual(requestTypeValue);
  //   expect(state.state.prospectType).toEqual(prospectTypeValue);

  //   expect(location.path).toHaveBeenCalledWith('/2/your-mortgage-needs');
  // });

  // it('vm.submit should not save data and go to dropout page', function() {
  //   /*--- Setup ---*/
  //   controller.data.requestType = killerRequestType;
  //   controller.data.prospectType = killerprospectType;
  //   var valid = true;
  //   controller.onSubmit(valid);
  //   // /*--- Test ---*/
  //   expect(location.path).toHaveBeenCalledWith('/0/placeholder');
  // });

});