/* global angular */

'use strict';

describe('UNIT: About You Controller', function() {

  var controller, form, state, location, rootScope;

  var data = {
    primary: {
      dateOfBirth: {
        day: "22",
        month: "04",
        year: "1989"
      },
      emailAddress: "test@blahdiblah.com",
      firstName: "Michael",
      income: {
        employmentStatus: "EMPLOYED"
      },
      outgoings: {
        numberOfDependants: "5"
      },
      surname: "Snow",
      title: "Mrs"
    }
  }

  beforeEach(function() {

    angular.mock.module('app');
    angular.mock.inject(function($rootScope, $controller, $templateCache, $compile, appState, $location, $q) {
      var scope = $rootScope.$new();
      controller = $controller('aboutYouController', {$scope: scope});
      controller.data = data;
      state = appState;
      rootScope = $rootScope;
      location = $location;
      spyOn(state, 'set').and.callThrough();
      spyOn(state, 'save').and.callFake(function() {
        var deferred = $q.defer();
        deferred.resolve('Promise Success');
        return deferred.promise;
      });
      spyOn(location, 'path').and.callThrough();
    });

  });

  it('should exist', function() {
    expect(controller).toBeDefined();
  });

  it('vm.onSubmit should prevent the default action', function() {
    var mockEvent = new Event('click');
    var isValid = true;
    spyOn(mockEvent, 'preventDefault');
    controller.onSubmit(mockEvent, isValid);
    /*--- Test ---*/
    expect(mockEvent.preventDefault).toHaveBeenCalled();
  });

  it('vm.onSubmit should save the data to appState', function() {
    /*--- Setup ---*/
    var mockEvent = new Event('click');
    var isValid = true;
    controller.onSubmit(mockEvent, isValid);
    expect(state.set).toHaveBeenCalled();
    expect(state.state.primary.title).toEqual(data.primary.title);
    expect(state.state.primary.surname).toEqual(data.primary.surname);
    expect(state.state.primary.firstName).toEqual(data.primary.firstName);
    expect(state.state.primary.emailAddress).toEqual(data.primary.emailAddress);
  });

  it('vm.submit should submit the form', function() {
    var mockEvent = new Event('click');
    var isValid = true;
    spyOn(mockEvent, 'preventDefault');
    controller.onSubmit(mockEvent, isValid);
    /*--- Test ---*/
    expect(state.save).toHaveBeenCalled();
  });

    it('Should move on the page location', function() {
    var mockEvent = new Event('click');
    var isValid = true;
    spyOn(mockEvent, 'preventDefault');
    controller.onSubmit(mockEvent, isValid);
    rootScope.$digest();
    /*--- Test ---*/
    expect(location.path).toHaveBeenCalledWith('/4/your-income');
  });

});