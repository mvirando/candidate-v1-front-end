// /* global angular */

// 'use strict';

// describe('UNIT: Your Mortgage Needs Spec', function() {

//   var controller, state, location;

//   var data = {
//     prospectType: 'REMORTGAGER',
//     propertyValue: '£100,000',
//     depositAmount: '£30,000',
//     requiredLoanAmount: '£70,000',
//     propertyFound: 'YES',
//     preferredMortgageTerm: 15
//   }

//   beforeEach(function() {

//     angular.mock.module('app');
//     angular.mock.inject(function($controller, $location, appState) {
//       appState.set({'prospectType': 'REMORTGAGER'});
//       controller = $controller('yourMortgageNeedsController');
//       controller.data = data;
//       location = $location;
//       state = appState;

//       spyOn(location, 'path').and.callThrough();
//     });

//   });

//   it('should exist', function() {
//     expect(controller).toBeDefined();
//   });

//   it('should have "REMORTGAGER" as the prospect type', function() {
//     expect(controller.data.prospectType).toEqual('REMORTGAGER');
//   });

//   it('vm.isRemortgager should return true', function() {
//     expect(controller.isRemortgager()).toBe(true);
//   });

//   it('vm.format should return a number', function() {
//     expect(controller.format(data.propertyValue)).toBe(100000);
//   });

//   it('vm.submit should post the data', function() {
//     /*--- Setup ---*/
//     var mockEvent = new Event('click');
//     var isValid = true;
//     spyOn(mockEvent, 'preventDefault');
//     controller.onSubmit(mockEvent, isValid);
//     /*--- test ---*/
//     expect(mockEvent.preventDefault).toHaveBeenCalled();
//     expect(state.state.propertyValue).toEqual(controller.format(data.propertyValue));
//     expect(state.state.depositAmount).toEqual(controller.format(data.depositAmount));
//     expect(state.state.requiredLoanAmount).toEqual(controller.format(data.requiredLoanAmount));
//     expect(state.state.propertyFound).toEqual(data.propertyFound);
//     expect(state.state.preferredMortgageTerm).toEqual(data.preferredMortgageTerm);

//     expect(location.path).toHaveBeenCalledWith('/3/about-you');
//   });

// });