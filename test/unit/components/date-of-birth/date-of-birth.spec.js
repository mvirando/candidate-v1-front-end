/*global angular, moment, module, browser*/

'use strict';

describe('Unit: Date of Birth Component', function() {

  var element, scope, $window, TEST_SCOPE;

  beforeEach(function() {
    angular.mock.module('app');
    angular.mock.inject(function($compile, _$rootScope_) {
      scope = _$rootScope_;
      scope.data = {
        day: '1',
        month: '1',
        year: '1990'
      };
      element = '<form name="TestForm"><div date-of-birth ng-model="data" context="TestForm"></form>';
      element = angular.element(element);
      $compile(element)(scope);
      scope.$digest();
      TEST_SCOPE = element.children().isolateScope();
    });
  });

  it('should have the earliest / latest date set ready for later use.', function() {
    var years = [],
        earliestYear = moment().subtract(68, 'years').year(),
        latestYear = moment().subtract(18, 'years').year();

    for (var i = earliestYear; i <= latestYear; i++) {
      years.push(i);
    }

    expect(TEST_SCOPE.dateData).toEqual({
      day: moment().date(),
      month: moment().month() + 1,
      years: years
    });
  });

  it('should return the date in string form', function() {
    var date = TEST_SCOPE.stringifyDate('1987','09','17');
    expect(date).toEqual('19870917');
  });

  describe('scope.checkDate should return the correct error message', function() {
    beforeEach(function() {
      spyOn(TEST_SCOPE, 'setError');
    });

    it('TOO OLD: should check the users date of birth', function() {

      var day = 1;
      var month = 1;
      var year = TEST_SCOPE.dateData.years[0];

      TEST_SCOPE.checkDate(TEST_SCOPE.stringifyDate(year, month, day));
      expect(TEST_SCOPE.setError).toHaveBeenCalledWith(false, 'old');
    });

    it('TOO YOUNG: should check the users date of birth', function() {
      var day = moment().date();
      var month = moment().month();
      var year = TEST_SCOPE.dateData.years[TEST_SCOPE.dateData.years.length - 1] + 1;

      TEST_SCOPE.checkDate(TEST_SCOPE.stringifyDate(year, month, day));
      expect(TEST_SCOPE.setError).toHaveBeenCalledWith(false, 'young');
    });

    it('FUTURE: should check the users date of birth', function() {
      var day = moment().date();
      var month = moment().month();
      var year = moment().year() + 1;

      TEST_SCOPE.checkDate(TEST_SCOPE.stringifyDate(year, month, day));
      expect(TEST_SCOPE.setError).toHaveBeenCalledWith(false, 'future');
    });

    it('ACCEPTED!: should check the users date of birth', function() {
      var day = moment().date();
      var month = moment().month();
      var year = 1987;
      TEST_SCOPE.checkDate(TEST_SCOPE.stringifyDate(year, month, day));
      expect(TEST_SCOPE.setError).toHaveBeenCalledWith(true, null);
    });

  });



});