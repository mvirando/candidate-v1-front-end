/*global angular, module, browser*/

'use strict';

describe('Unit: Backbutton', function() {

  var element, scope, $window

  beforeEach(function() {
    angular.mock.module('app');
    angular.mock.inject(function($compile, $rootScope, _$window_) {
      $window = _$window_;
      scope = $rootScope;
      element = angular.element('<a href="#" back-button>Back Button</a>');
      $compile(element)(scope);
      scope.$digest();
    });
  });

  it('should call $window.history.back()', function() {
    spyOn($window.history, 'back');
    element.triggerHandler('click');
    expect($window.history.back).toHaveBeenCalled();
  });

});