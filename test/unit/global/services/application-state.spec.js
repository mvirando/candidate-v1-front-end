// /*global angular */

// 'use strict';

// describe('Unit: Application State Service', function() {

//   var http, service;

//   var defaultState = {
//     uid: null,
//     interactions: {},
//     primary: {},
//     secondary: null
//   };

//   beforeEach(function() {
//     // instantiate the app module
//     angular.mock.module('app');

//     // mock the service
//     angular.mock.inject(function(appState) {
//       service = appState;
//     });

//   });

//   afterEach(function() {
//     service = null;
//   });

//   it('should exist', function() {
//     expect(service).toBeDefined();
//   });

//   it('should have a default state', function() {
//     expect(service.state).toEqual(defaultState);
//   });

//   describe('SET: ', function() {

//     it('should "set" a new property on the state.', function() {
//       service.set({ test: 'test' });
//       expect(service.state.test).toEqual('test');
//     });

//     it('should "set" a new property on object property', function() {
//       service.set({ test: 'test' }, 'interactions');
//       expect(service.state.interactions.test).toEqual('test');
//     });

//   });

//   describe('GET: ', function() {

//     it('should "GET" the value from the property.', function() {
//       service.set({ test: 'test' });
//       var getTest = service.get('test');
//       expect(getTest).toEqual('test');
//     });

//     it('should "GET" the value from a deep property.', function() {
//       service.set({test: 'test'}, 'interactions');
//       var getTest = service.get('interactions', 'test');
//       expect(getTest).toEqual('test');
//     });

//   });

//   describe('FORMAT DATA: ', function() {

//     it('should return the data in sendable format', function() {
//       service.set({ testOnState: 'test'})
//              .set({ testOnDeepObject: 'test'}, 'interactions');

//       var data = service.formatData(['testOnState', 'interactions.testOnDeepObject']);

//       expect(data).toEqual({
//         testOnState: 'test',
//         interactions: {
//           testOnDeepObject: 'test'
//         }
//       });
//     });

//   });

// });