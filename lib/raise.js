'use strict';
var announce = require('./announce');

module.exports = function(evt) {
    announce(evt);
    if (evt instanceof Error) {
        throw (evt);
    }
};
