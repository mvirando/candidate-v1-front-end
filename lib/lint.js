'use strict';
var
    gulp = require('gulp'),
    lint = require('gulp-jshint'),
    stylish = require('jshint-stylish');

module.exports = function() {
    return gulp.src(['./src/scripts/**/!(*spec|*mock).js', './src/app/**/*.js'])
        .pipe(lint())
        .pipe(lint.reporter(stylish));
};
