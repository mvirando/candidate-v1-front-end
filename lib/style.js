'use strict';
var
    gulp  = require('gulp'),
    style = require('gulp-jscs');

module.exports = function() {
    return gulp.src(['./src/scripts/**/!(*spec|*mock).js', './src/app/**/*.js'])
        .pipe(style());
};
