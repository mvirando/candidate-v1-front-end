'use strict';
var
    q        = require('q'),
    fs       = require('fs'),
    raise    = require('./raise'),
    announce = require('./announce');

module.exports = function(version) {
    var
        packageFileName = './package.json',
        writePackage = function(thePackage) {
            var deferred = q.defer();

            fs.writeFile(packageFileName, JSON.stringify(thePackage, null, 2), function(err) {
                if (err) {
                    deferred.reject(err);
                }
                announce('new package version is \'' + thePackage.version + '\'');
                deferred.resolve(thePackage);
            });

            return deferred.promise;
        },
        setVersion = function(thePackage) {
            announce('setting the package version number');
            announce('current package version is \'' + thePackage.version + '\'');

            thePackage.version = version.major + '.' + version.minor + '.' + version.revision;

            return q.resolve(thePackage);
        },
        readPackage = function() {
            var deferred = q.defer();

            fs.readFile(packageFileName, function(err, packageData) {
                if (err) {
                    deferred.reject(err);
                }

                deferred.resolve(JSON.parse(packageData));
            });

            return deferred.promise;
        };

    readPackage()
        .then(setVersion)
        .then(writePackage)
        .fail(function(err) {
            raise(err);
        });
};
