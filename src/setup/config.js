module.exports = ['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'views/landing/landing.html',
      controller: 'landingController',
      controllerAs: 'vm'
    })
    .when('/terms-and-conditions', {
      templateUrl: 'views/terms-and-conditions/terms-and-conditions.html',
      controller: 'termsAndConditionsController',
      controllerAs: 'vm',
      resolve: {
        user: ['auth', (auth) => {
          return auth.get();
        }]
      }
    })
    .when('/dashboard', {
      templateUrl: 'views/dashboard/dashboard.html',
      controller: 'dashboardController',
      resolve: {
        user: ['auth', (auth) => {
          return auth.get();
        }]
      },
      controllerAs: 'vm'
    })
    .when('/income', {
      templateUrl: 'views/income/income.html',
      controller: 'incomeController',
      controllerAs: 'vm',
      resolve: {
        user: ['auth', (auth) => {
          return auth.get();
        }]
      }
    })
    .when('/chat', {
      templateUrl: 'views/chat/chat.html',
      controller: 'chatController',
      controllerAs: 'vm',
      resolve: {
        user: ['auth', (auth) => {
          return auth.get();
        }]
      }
    })
    .when('/jobs', {
      templateUrl: 'views/jobs/jobs.html',
      controller: 'jobsController',
      controllerAs: 'vm',
      resolve: {
        user: ['auth', (auth) => {
          return auth.get();
        }]
      }
    })
    .when('/settings', {
      templateUrl: 'views/settings/settings.html',
      controller: 'settingsController',
      controllerAs: 'vm',
      resolve: {
        user: ['auth', (auth) => {
          return auth.get();
        }]
      }
    })
    .when('/roles/role/:id', {
      templateUrl: 'views/single-role/single-role.html',
      controller: 'singleRoleController',
      controllerAs: 'vm',
      resolve: {
        user: ['auth', (auth) => {
          return auth.get();
        }]
      }
    })
    .otherwise({redirectTo: '/'});

  $httpProvider.defaults.withCredentials = true;

}];
