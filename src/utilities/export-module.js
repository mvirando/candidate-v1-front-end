'use strict';

var angular = require('angular'),
    moduleToExport;

function declare(componentMap) {
  Object.keys(componentMap).forEach(function(key) {

    var item = componentMap[key];

    if (!item) {
      return;
    }

    if (item.fn && typeof angular.isFunction(item.fn)) {
      switch (item.type) {
        case 'controller': {
          moduleToExport.controller(item.name, item.fn);
          break;
        }
        case 'service': {
          moduleToExport.service(item.name, item.fn);
          break;
        }
        case 'factory': {
          moduleToExport.factory(item.name, item.fn);
          break;
        }
        case 'filter': {
          moduleToExport.filter(item.name, item.fn);
          break;
        }
        case 'constant': {
          moduleToExport.constant(item.name, item.fn);
          break;
        }
        default: {
          moduleToExport.directive(item.name, item.fn);
          break;
        }
      }
    } else {
      declare(item);
    }
  });
}

module.exports = function(name, components) {
  moduleToExport = angular.module(name, []);
  declare(components);
  return moduleToExport;
};
