const dashboardController = [
  '$log',
  'auth',
  '$location',
  'rolesService',
  'utilities',
  function($log, auth, $location, rolesService, utilities) {
    const vm = this;
    let url;

    if (auth.user) {
      vm.user = auth.user;

      url = vm.user.userType === 'Freelancer' ? 'roles/search' : 'roles/all';

      rolesService.getRoles(utilities.createUrl(url)).then(res => {
        vm.roles = res.data.roles;
        $log.log('vm.roles', vm.roles);
      });

    }

  }
];

export default {
  name: 'dashboardController',
  fn: dashboardController,
  type: 'controller'
};
