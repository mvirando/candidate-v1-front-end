const settingsController = [
  '$log',
  'auth',
  '$location',
  function($log, auth, $location) {

    const vm = this;

    vm.data = {};

    vm.user = auth;

    // if (vm.user.isFreelancer()) {
    //   vm.data = Object.assign({}, vm.data, {_freelancer: {}});
    // }

    vm.onSubmit = ($ev, $valid) => {

      $ev.preventDefault();

      console.log(vm.data);

    };

  }
];

export default {
  name: 'settingsController',
  fn: settingsController,
  type: 'controller'
};
