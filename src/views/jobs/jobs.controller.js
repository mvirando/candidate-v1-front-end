const jobsController = [
  '$log',
  'auth',
  '$location',
  'rolesService',
  'utilities',
  function($log, auth, $location, rolesService, utilities) {
    const vm = this;

    vm.user = auth;

    if (!auth.isFreelancer()) {
      $log.log(true);
    }

    vm.getRoles = ($ev, url) => {
      $ev.preventDefault();
      rolesService.getRoles(utilities.createUrl(`roles/${url}`)).then(res => {
        vm.roles = res.data.applications;
      });
    };

  }
];

export default {
  name: 'jobsController',
  fn: jobsController,
  type: 'controller'
};
