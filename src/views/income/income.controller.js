import moment from 'moment';

const incomeController = [
  '$log',
  '$location',
  'timesheetsService',
  'auth',
  'utilities',
  '$http',
  function($log, $location, timesheetsService, auth, utilities, $http) {

    const vm = this;

    vm.user = auth;

    const url = auth.isFreelancer() ? 'timesheets' : 'timesheets-company';

    $http.get(utilities.createUrl(url)).then(res => {

      if (!res.data.error) {

        vm.returned = res.data;
        vm.data = auth.isFreelancer() ? res.data.application.timesheets : res.data.applications;

        $log.log('vm.data', vm.data);

      } else {

      }

    });

  }
];

export default {
  name: 'incomeController',
  fn: incomeController,
  type: 'controller'
};
