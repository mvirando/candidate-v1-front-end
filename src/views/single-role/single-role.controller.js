const singleRoleController = [
  '$log',
  'auth',
  '$location',
  'utilities',
  'rolesService',
  function($log, auth, $location, utilities, rolesService) {
    const vm = this;
    rolesService.getSingleRole().then(res => {
      vm.data = res.role;
    });
  }
];

export default {
  name: 'singleRoleController',
  fn: singleRoleController,
  type: 'controller'
};
