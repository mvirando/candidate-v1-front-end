const chatController = [
  '$log',
  'auth',
  '$location',
  'chatService',
  function($log, auth, $location, chatService) {
    const vm = this;
    chatService.fetchConversations();
  }
];

export default {
  name: 'chatController',
  fn: chatController,
  type: 'controller'
};
