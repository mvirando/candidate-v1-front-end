const landingController = [
  '$log',
  'auth',
  '$location',
  function($log, auth, $location) {
    const vm = this;
    // On Login submit...
    vm.onSubmit = ($ev, $valid) => {
      // temp
      $ev.preventDefault();
      if ($valid) {
        auth.login(vm.data).then(res => {
          vm.error = false;
          let path;
          if (res.status === 200) {
            if (res.data.user.acceptedTerms) {
              path = '/dashboard';
            } else {
              path = '/terms-and-conditions';
            }
            $location.path(path);
          } else {
            vm.error = res.data.error;
          }
        }, err => {
          $log.log('err', err);
        });
      }
    };

  }
];

export default {
  name: 'landingController',
  fn: landingController,
  type: 'controller'
};
