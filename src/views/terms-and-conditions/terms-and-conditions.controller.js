const termsAndConditionsController = [
  '$log',
  'auth',
  '$location',
  function($log, auth, $location) {
    const vm = this;

    vm.acceptTerms = () => {
      auth.acceptTerms().then(res => {

      });
    };
  }
];

export default {
  name: 'termsAndConditionsController',
  fn: termsAndConditionsController,
  type: 'controller'
};
