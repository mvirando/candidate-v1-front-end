'use strict';

import angular from 'angular';
import config from './setup/config';
import run from './setup/run';

let requires;

// Require all the modules...
import 'angular-route';
import 'angular-messages';
import './templates';
import './global/controllers';
import './global/services';
import './global/directives';
import './global/filters';
import './global/constants';
import './views';
import './components';

requires = [
  'ngRoute',
  'app.controllers',
  'app.services',
  'app.directives',
  'app.filters',
  'app.components',
  'app.constants',
  'app.views',
  'ngMessages',
  'templates'
];

angular.module('app', requires).config(config).run(run);

angular.bootstrap(document, ['app'], {
  strictDi: true
});
