import _ from 'underscore';
import angular from 'angular';

const utilities = [
  '$log',
  '$http',
  '$location',
  function($log, $http, $location) {
    // 178.62.73.251
    this.createUrl = str => `http://178.62.73.251:8000/api/${str}`;
    this.handleError = err => {
      $log.log(err);
      $location.path('/');
    };
  }];

export default {
  name: 'utilities',
  fn: utilities,
  type: 'service'
};
