import angular from 'angular';

const service = function($log, $http, auth, utilities) {

  function success (res) {
    return res;
  };

  function error (err) {
    $log.log('err', err);
    // alert();
  };

  this.createConversation = id => {

    if (!auth.isFreelancer() && auth.user) {
      return $http.post(utilities.createUrl(`${id}/conversation/create`), {})
        .then(res => res, err => error);
    }

  };

  this.fetchConversations = () => {

    $http.get(utilities.createUrl('conversations/list')).then(res => {
      $log.log('res', res);
    }, err => err);

  };

};

const chatService = ['$log', '$http', 'auth', 'utilities', service];

export default {
  name: 'chatService',
  fn: chatService,
  type: 'service'
};
