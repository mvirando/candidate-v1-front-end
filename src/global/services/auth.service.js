import _ from 'underscore';
import angular from 'angular';

const authService = [
  '$log',
  '$http',
  'utilities',
  '$location',
  function($log, $http, utilities, $location) {

    this.returnUser = () => {
    };

    this.get = () => {
      return $http.get(utilities.createUrl('user/'), {handleError: true}).then(res => {
        const {user} = res.data;
        if (!user) {
          $location.path('/');
        } else {
          this.user = user;
        }
        return res;
      }, err => {
        utilities.handleError(err);
        return err;
      });
    };

    this.isFreelancer = () => this.user && this.user.userType === 'Freelancer';

    this.login = data => {

      return $http.post(utilities.createUrl('user/login'), data).then(res => {
        this.user = res.data.user;
        $log.log('Logged in user: ', this.user);
        return res;
      }, err => err);

    };

    this.acceptTerms = () => {
      return $http.put(utilities.createUrl('user'),
        {acceptedTerms: true}
      ).then(res => {
        if (res.status === 200) {
          $location.path('/dashboard');
        }
      }, err => {
        $log.log('err', err);
      });
    };

    this.logout = () => {
      return $http.post(utilities.createUrl('user/logout')).then(res => {
        $location.path('/');
      });
    };

  }];

export default {
  name: 'auth',
  fn: authService,
  type: 'service'
};
