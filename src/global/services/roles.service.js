import _ from 'underscore';
import angular from 'angular';

const rolesService = [
  '$log',
  '$http',
  'utilities',
  '$routeParams',
  function($log, $http, utilities, $routeParams) {
    this.getRoles = (url) => {
      $log.log('url', url);
      return $http.get(url, {withCredentials: true}).then(res => {
        $log.log('res', res);
        return res;
      }, err => err);
    };
    this.applyForRole = id => {
      return $http.post(utilities.createUrl(`roles/role/apply/${id}`)).then(res => {
        return res;
      }, err => err);
    };
    this.saveRole = id => {
      return $http.post(utilities.createUrl(`roles/role/save/${id}`)).then(res => {
        return res;
      }, err => err);
    };
    this.getSingleRole = () => {
      return $http.get(utilities.createUrl(`roles/role/single/${$routeParams.id}`)).then(res => {
        if (!res.data.error) {
          return res.data;
        }
        alert(res.data.message);
      }, err => err);
    };
  }];

export default {
  name: 'rolesService',
  fn: rolesService,
  type: 'service'
};
