import _ from 'underscore';
import angular from 'angular';

const applicationStateService = [
  '$log',
  '$http',
  function($log, $http) {
  }];

export default {
  name: 'appState',
  fn: applicationStateService,
  type: 'service'
};
