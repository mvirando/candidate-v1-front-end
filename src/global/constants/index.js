import angular from 'angular';
import exportModule from '../../utilities/export-module';
const bulk = require('bulk-require');
const importedConstants = bulk(__dirname, ['./**/!(*index|*.spec).js']);

export default exportModule('app.constants', importedConstants);
