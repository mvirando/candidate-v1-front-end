const linksConstants = [
  {route: '#/dashboard', name: 'search', icon: 'search'},
  {route: '#/jobs', name: 'myJobs', icon: 'jobs'},
  {route: '#/chat', name: 'chat', icon: 'chat'},
  {route: '#/income', name: 'income', icon: 'timesheet'},
  {route: '#/settings', name: 'settings', icon: 'settings'}
];

export default {
  name: 'linksConstants',
  fn: linksConstants,
  type: 'constant'
};
