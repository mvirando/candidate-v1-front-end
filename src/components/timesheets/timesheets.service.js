import _ from 'underscore';
import angular from 'angular';
import moment from 'moment';

const timesheetsService = [
  '$log',
  '$http',
  'utilities',
  'auth',
  function($log, $http, utilities, auth) {

    this.datesData = [];

    this.format = 'Do MMM YYYY';

    this.formatValue = (obj) => `${obj.hours}:${obj.minutes}`;

    this.currentData = (start) => {

      this.datesData = {
        approved: false,
        submitted: false,
        data: []
      };

      // This is breaking everything...
      let day = start;

      const endOfFormat = (date, format) => date.endOf('day').format(format);

      const findValues = date => {

        let obj = {};

        _.each(this.timesheets, timesheet => {

          if (moment(timesheet.date).isSame(date)) {

            obj.value = this.formatValue(timesheet.time);
            obj.status = timesheet.status;

            if (timesheet.status.approved) {
              this.datesData.approved = true;
            }

            if (timesheet.status.submitted) {
              this.datesData.submitted = true;
            }

          }

        });

        return obj;

      };

      _.times(7, i => {

        let obj = {};

        if (!i) {

          obj.date = endOfFormat(day, this.format);
          obj.timestamp = endOfFormat(day, 'x');
          obj = Object.assign({}, obj, findValues(day));
          this.datesData.start = obj.date;

        } else {

          var newVal = day.add(1, 'days');
          obj.date = endOfFormat(newVal, this.format);
          obj.timestamp = endOfFormat(newVal, 'x');
          obj = Object.assign({}, obj, findValues(newVal));

          if (i === 6) {
            this.datesData.end = obj.date;
          }

        }

        this.datesData.data.push(obj);

      });

    };

    this.setTimescales = dir => {

      var start = moment(this.start, this.format);

      if (dir === 'prev') {
        start = start.subtract(1, 'week');
      } else {
        start = start.add(1, 'week');
      }

      /* HACK */
      this.start = start.format(this.format);

      this.currentData(start);

    };

    this.setInitialData = (timesheets) => {

      this.timesheets = timesheets;

      /* HACK */

      var start = moment().startOf('isoWeek');
      var formatted = start.format(this.format);

      this.currentData(start);

      this.start = formatted;
    };

    this.createOrUpdate = (data) => {

      return $http.post(utilities.createUrl('timesheets'), data).then(res => {

        const {timesheet} = res.data;
        const index = _.findIndex(this.timesheets, {_id: timesheet._id});

        if (index === -1) {
          this.timesheets.push(timesheet);
        } else {
          this.timesheets[index] = timesheet;
        }
        return res;
      }, err => {
        $log.log('err', err);
      });
    };

    this.submitApproveTimesheet = type => {

      const start = moment(this.start, this.format);

      const data = {
        start: start.format('x'),
        end: start.add(7, 'days').format('x')
      };

      return $http.post(utilities.createUrl(`timesheets/${type}`), data).then(res => {
        if (!res.data.error) {
          if (type === 'approve') {
            this.datesData.approved = true;
          } else {
            this.datesData.submitted = true;
          }
          return res;
        }
        alert(res.data.message);
      });

    };

  }];

export default {
  name: 'timesheetsService',
  fn: timesheetsService,
  type: 'service'
};
