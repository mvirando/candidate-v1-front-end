import angular from 'angular';
import _ from 'underscore';
import moment from 'moment';

const TimesheetsDirecitiveCtrl = ['$log', 'timesheetsService', '$scope', 'auth', function($log, timesheetsService, $scope, auth) {

  const blurEnabled = () => this.user.isFreelancer() && !this.data.datesData.approved;

  $scope.$watch('timesheetsVm.sheets', newVal => {
    if (newVal) {
      timesheetsService.setInitialData(this.sheets);
    }
  });

  this.user = auth;

  this.isReadOnly = () => blurEnabled() ? '' : 'readonly';

  this.data = timesheetsService;

  this.setTimescales = ($ev,dir) => {
    $ev.preventDefault();
    timesheetsService.setTimescales(dir);
  };

  this.onBlur = ($ev, day) => {

    if (blurEnabled()) {

      const data = {
        date: day.timestamp,
        time: angular.element($ev.target).val()
      };

      if (data.time !== '') {
        timesheetsService.createOrUpdate(data).then(res => {
          $log.log(res.data.message);
        });
      }
    }

  };

  this.action = type => {
    timesheetsService.submitApproveTimesheet(type).then(res => {
      if (res.data.error) {
        alert(`Error: ${res.data.message}`);
      }
    });
  };

}];

const timesheets = ['$window', '$log', 'timesheetsService',
  function($window, $log) {
  return {
    restrict: 'E',
    scope: {
      sheets: '='
    },
    templateUrl: 'components/timesheets/timesheets.html',
    controller: TimesheetsDirecitiveCtrl,
    controllerAs: 'timesheetsVm',
    bindToController: true,
    replace: true
  };
}];

export default {
  name: 'timesheets',
  fn: timesheets,
  type: 'directive'
};
