import angular from 'angular';
import _ from 'underscore';

const chatComponent = ['$window', '$log',
  function($window, $log) {
  return {
    restrict: 'E',
    templateUrl: 'components/chat/chat.html',
    replace: true
  };
}];

export default {
  name: 'chat',
  fn: chatComponent,
  type: 'directive'
};
