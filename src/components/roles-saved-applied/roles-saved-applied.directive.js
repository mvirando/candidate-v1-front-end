import angular from 'angular';
import _ from 'underscore';

const rolesSavedApplied = ['$window', '$log',
  function($window, $log) {
  return {
    restrict: 'E',
    templateUrl: 'components/roles-saved-applied/roles-saved-applied.html',
    replace: true,
    scope: {
      data: '='
    }
  };
}];

export default {
  name: 'rolesSavedApplied',
  fn: rolesSavedApplied,
  type: 'directive'
};
