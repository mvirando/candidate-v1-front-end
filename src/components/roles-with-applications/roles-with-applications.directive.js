import angular from 'angular';
import _ from 'underscore';

const controller = ['$log', '$location', 'chatService', function($log, $location, chatService) {
  this.createChat = id => {
    chatService.createConversation(id).then(res => {
      $log.log('res: in roles', res);
    });
  };
}];

function directive($window, $log) {
  return {
    templateUrl: 'components/roles-with-applications/roles-with-applications.html',
    scope: {data: '='},
    controller: controller,
    controllerAs: 'rolesWithAppsVm',
    bindToController: true
  };
};

const rolesWithApplications = ['$window', '$log', directive];

export default {
  name: 'rolesWithApplications',
  fn: rolesWithApplications,
  type: 'directive'
};
