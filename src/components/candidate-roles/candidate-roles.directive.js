import angular from 'angular';
import _ from 'underscore';

const candidateRolesCtrl = ['$log', 'rolesService', function($log, rolesService) {

  this.applyForRole = id => {
    rolesService.applyForRole(id).then(res => {
      let index = _.findIndex(this.data, {_id: id});
      this.data[index].hasApplied = true;
      this.data[index].hasSaved = true;
    });
  };

  this.saveRole = id => {
    rolesService.saveRole(id).then(res => {
      let index = _.findIndex(this.data, {_id: id});
      this.data[index].hasSaved = !this.data[index].hasSaved;
    });
  };

}];

const candidateRolesDirective = [function() {
  return {
    scope: {
      data: '=',
      filterBy: '='
    },
    controller: candidateRolesCtrl,
    controllerAs: 'candidateRolesVm',
    bindToController: true,
    templateUrl: 'components/candidate-roles/candidate-roles.html'
  };
}];

export default {
  name: 'candidateRoles',
  fn: candidateRolesDirective,
  type: 'directive'
};
