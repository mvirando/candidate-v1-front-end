import angular from 'angular';
import _ from 'underscore';

const loader = ['$window', '$log', 'linksConstants', 'auth',
  function($window, $log, linksConstants, auth) {
  return {
    restrict: 'E',
    template: '<div class="loader"></div>',
    replace: true
  };
}];

export default {
  name: 'loader',
  fn: loader,
  type: 'directive'
};
