import angular from 'angular';
import _ from 'underscore';

function controller($log, linksConstants, auth, $location) {
  // Fetch the links...
  this.links = linksConstants;
  // Logout
  this.logout = $ev => {
    $ev.preventDefault();
    auth.logout();
  };
  this.isActive = path => path.split('#')[1] === $location.path();
};

const navCtrl = ['$log', 'linksConstants', 'auth', '$location', controller];

const navigationDirective = [
  function() {
  return {
    restrict: 'E',
    templateUrl: 'components/global/navigation/navigation.html',
    controller: navCtrl,
    controllerAs: 'navVm',
    bindToController: true,
    replace: true
  };
}];

export default {
  name: 'navigation',
  fn: navigationDirective,
  type: 'directive'
};
