import angular from 'angular';
import _ from 'underscore';

const userHeaderDirective = ['$window', '$log', 'linksConstants', 'auth',
  function($window, $log, linksConstants, auth) {
  return {
    restrict: 'E',
    templateUrl: 'components/global/user-header/user-header.html',
    replace: true,
    scope: {
      data: '='
    }
    // link: (scope, element, attrs) => {
    // }
  };
}];

export default {
  name: 'userHeader',
  fn: userHeaderDirective,
  type: 'directive'
};
